import fs from "fs";

enum HandType {
  FiveOFAKind,
  FourOFAKind,
  FullHouse,
  ThreeOFAKind,
  TwoPair,
  OnePair,
  HighCard,
}

type Hand = {
  cards: string[];
  type: HandType;
  bid: number;
  rank?: number;
};

const isNumeric = (s: string) => !isNaN(Number(s));

const readlines = (path: string) => {
  return fs
    .readFileSync(path, { encoding: "utf8" })
    .split("\n")
    .filter((l) => l.length > 0);
};

const lines = readlines("../inputs/day7.txt");

const handsP1: Hand[] = lines.map((l) => {
  const [cards, bid] = l.split(" ");

  const cardAmounts: Record<string, number> = {};
  cards.split("").forEach((c) => {
    cardAmounts[c] = (cardAmounts[c] || 0) + 1;
  });

  const maxSameCards = Math.max(...Object.values(cardAmounts));

  let type;
  switch (maxSameCards) {
    case 5:
      type = HandType.FiveOFAKind;
      break;
    case 4:
      type = HandType.FourOFAKind;
      break;
    case 3:
      type = Object.values(cardAmounts).includes(2)
        ? HandType.FullHouse
        : HandType.ThreeOFAKind;
      break;
    case 2:
      type =
        Object.values(cardAmounts).filter((a) => a === 2).length === 2
          ? HandType.TwoPair
          : HandType.OnePair;
      break;
    case 1:
    default:
      type = HandType.HighCard;
  }

  return { cards: cards.split(""), type, bid: Number(bid) };
});

const orderingP1 = (a: Hand, b: Hand) => {
  if (a.type === b.type) {
    for (let i = 0; i < 5; i++) {
      const acard = a.cards[i];
      const bcard = b.cards[i];

      if (acard === bcard) continue;

      if (isNumeric(acard) && isNumeric(bcard)) {
        return Number(acard) < Number(bcard) ? -1 : 1;
      }

      if (isNumeric(acard) || isNumeric(bcard)) {
        return isNumeric(acard) ? -1 : 1;
      }

      if (acard !== "J" && bcard !== "J") {
        return acard.codePointAt(0)! < bcard.codePointAt(0)! ? 1 : -1;
      }

      if (acard === "J" && bcard === "T") return 1;

      if (bcard === "J" && acard !== "T") return 1;

      return -1;
    }

    return 0;
  }

  return a.type < b.type ? 1 : -1;
};

const handsWithRankP1 = [...handsP1]
  .sort(orderingP1)
  .map((h, i) => ({ ...h, rank: i + 1 }));

const answerP1 = handsWithRankP1.reduce(
  (acc, hand) => acc + hand.bid * hand.rank,
  0,
);

console.log("Part 1: " + answerP1);

const handsP2: Hand[] = lines.map((l) => {
  const [cards, bid] = l.split(" ");

  const cardAmounts: Record<string, number> = {};
  cards.split("").forEach((c) => {
    cardAmounts[c] = (cardAmounts[c] || 0) + 1;
  });

  const { J: jokers, ...cardsWithoutJoker } = cardAmounts;
  const maxSameCards = Math.max(...Object.values(cardsWithoutJoker));

  let type;
  switch ([maxSameCards, jokers || 0].toString()) {
    case "5,0":
    case "4,1":
    case "3,2":
    case "2,3":
    case "1,4":
    case "0,5":
      type = HandType.FiveOFAKind;
      break;
    case "4,0":
    case "3,1":
    case "2,2":
    case "1,3":
      type = HandType.FourOFAKind;
      break;
    case "3,0":
    case "1,2":
      type = Object.values(cardsWithoutJoker).includes(2)
        ? HandType.FullHouse
        : HandType.ThreeOFAKind;
      break;
    case "2,1":
      type =
        Object.values(cardsWithoutJoker).filter((a) => a === 2).length === 2
          ? HandType.FullHouse
          : HandType.ThreeOFAKind;
      break;
    case "2,0":
    case "1,1":
      type =
        Object.values(cardsWithoutJoker).filter((a) => a === 2).length === 2
          ? HandType.TwoPair
          : HandType.OnePair;
      break;
    case "1,0":
      type = HandType.HighCard;
      break;
    default:
      type = HandType.FiveOFAKind;
  }

  return { cards: cards.split(""), type, bid: Number(bid) };
});

const orderingP2 = (a: Hand, b: Hand) => {
  if (a.type === b.type) {
    for (let i = 0; i < 5; i++) {
      const acard = a.cards[i];
      const bcard = b.cards[i];

      if (acard === bcard) continue;

      if (acard === "J") return -1;
      if (bcard === "J") return 1;

      if (isNumeric(acard) && isNumeric(bcard)) {
        return Number(acard) < Number(bcard) ? -1 : 1;
      }

      if (isNumeric(acard) || isNumeric(bcard)) {
        return isNumeric(acard) ? -1 : 1;
      }

      return acard.codePointAt(0)! < bcard.codePointAt(0)! ? 1 : -1;
    }

    return 0;
  }

  return a.type < b.type ? 1 : -1;
};

const handsWithRankP2 = [...handsP2]
  .sort(orderingP2)
  .map((h, i) => ({ ...h, rank: i + 1 }));

const answerP2 = handsWithRankP2.reduce(
  (acc, hand) => acc + hand.bid * hand.rank,
  0,
);

console.log("Part 2: " + answerP2);
