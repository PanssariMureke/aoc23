import fs from "fs";

// right down left up
const directions = [
  [1, 0],
  [0, 1],
  [-1, 0],
  [0, -1],
];
const dirTranslation = new Map([
  ["0,-", 0],
  ["0,7", 1],
  ["0,J", 3],
  ["1,|", 1],
  ["1,L", 0],
  ["1,J", 2],
  ["2,-", 2],
  ["2,L", 3],
  ["2,F", 1],
  ["3,|", 3],
  ["3,7", 2],
  ["3,F", 0],
]);

const readlines = (path: string) => {
  return fs
    .readFileSync(path, { encoding: "utf8" })
    .split("\n")
    .filter((l) => l.length > 0);
};

const lines = readlines("./inputs/day10.txt");

const grid = lines.map((line) => line.split(""));

let animalX = 0;
let animalY = 0;
grid.forEach((line, i) =>
  line.forEach((c, j) => {
    if (c === "S") {
      animalX = j;
      animalY = i;
    }
  }),
);

const startDirections = [
  "-7J".includes(grid[animalY][animalX + 1]),
  "|LJ".includes(grid[animalY + 1][animalX]),
  "-LF".includes(grid[animalY][animalX - 1]),
  "|7F".includes(grid[animalY - 1][animalX]),
]
  .map((v, i) => (v ? i : -1))
  .filter((i) => i !== -1);

let currentDirection = startDirections[0];
let currentX = animalX + directions[currentDirection][0];
let currentY = animalY + directions[currentDirection][1];
let answerP1 = 1;

const gridP2 = Array.from(Array(grid.length), () =>
  Array(grid[0].length).fill(false),
);
gridP2[animalY][animalX] = true;

while (currentX !== animalX || currentY !== animalY) {
  answerP1++;
  gridP2[currentY][currentX] = true;
  currentDirection = dirTranslation.get(
    `${currentDirection},${grid[currentY][currentX]}`,
  )!;
  const [dx, dy] = directions[currentDirection];
  currentX += dx;
  currentY += dy;
}

answerP1 = Math.floor(answerP1 / 2);

console.log("Part 1: " + answerP1);

let answerP2 = 0;

const indexes: [number, number][] = [];
for (let y = 0; y < grid.length; y++) {
  let inTheLoop = false;
  for (let x = 0; x < grid[0].length; x++) {
    if (gridP2[y][x]) {
      if (
        "|JL".includes(grid[y][x]) ||
        (grid[y][x] === "S" && startDirections.includes(3))
      ) {
        indexes.push([x, y]);
        inTheLoop = !inTheLoop;
      }
    } else {
      answerP2 += Number(inTheLoop);
    }
  }
}

console.log("Part 2: " + answerP2);
