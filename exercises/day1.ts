import fs from "fs";

const readlines = (path: string) => {
  return fs.readFileSync(path, { encoding: "utf8" }).split("\n");
};

const lines = readlines("../inputs/day1.txt");

// const numbers_part1 = lines.map(line => {
//   const finds = Array.from(line.matchAll(/[0-9]/g))
//   const first = finds[0];
//   const last = finds[finds.length - 1];
//   if (first === undefined) {
//     return 0;
//   }
//   return Number(first[0]) * 10 + Number(last[0])
// })
//
// console.log(numbers_part1.reduce((a, b) => a + b));

const lookup: Record<string, number> = {
  one: 1,
  two: 2,
  three: 3,
  four: 4,
  five: 5,
  six: 6,
  seven: 7,
  eight: 8,
  nine: 9,
  zero: 0,
};

const numbers_part2 = lines.map((line) => {
  const finds = Array.from(
    line.matchAll(
      /(?=(zero|one|two|three|four|five|six|seven|eight|nine|[0-9]))/g,
    ),
  );
  const first = finds[0];
  const last = finds[finds.length - 1];
  if (first === undefined) {
    return 0;
  }
  const first_num = isNaN(Number(first[1]))
    ? lookup[first[1]]
    : Number(first[1]);
  const last_num = isNaN(Number(last[1])) ? lookup[last[1]] : Number(last[1]);
  return first_num * 10 + last_num;
});

console.log(numbers_part2.reduce((a, b) => a + b));
