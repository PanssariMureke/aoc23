import fs from "fs";

type Mapping = {
  src: number;
  dest: number;
  range: number;
}[];

const mapVariable = (variable: number, mapping: Mapping) => {
  const found = mapping.find(
    (m) => m.src <= variable && m.src + m.range > variable,
  );
  return found ? variable - found.src + found.dest : variable;
};

const input = fs.readFileSync("../inputs/day5.txt", { encoding: "utf8" });
const sections = input.split("\n\n");

// const [
//   seedToSoil,
//   soilToFertilizer,
//   fertilizerToWater,
//   waterToLight,
//   lightToTemperature,
//   temperatureToHumidity,
//   humidityToLocation,
// ] =
const mappings = sections.slice(1).map<Mapping>((section) => {
  const lines = section.split("\n").slice(1);
  return lines.map((line) => {
    const [dest, src, range] = line.split(" ").map((n) => Number(n));
    return { src, dest, range };
  });
});

const seeds_part1 = sections[0]
  .split("\n")[1]
  .split(" ")
  .map((n) => Number(n));

const findLocation = (seed: number) =>
  mappings.reduce((output, mapping) => mapVariable(output, mapping), seed);

const answer_part1 = seeds_part1.reduce((prev, seed) =>
  Math.min(prev, findLocation(seed)),
);

console.log("Part 1: " + answer_part1);

const seeds_part2 = Array.from(
  sections[0].split("\n")[1].matchAll(/[0-9]+ [0-9]+/g),
).map((r) => {
  const [from, amount] = r[0].split(" ");
  return { from: Number(from), amount: Number(amount) };
});

const makeIterator = (ranges: { from: number; amount: number }[]) => {
  let i = 0;
  let j = ranges[0].from;
  const iteratorLength = ranges.reduce((a, b) => a + b.amount, 0);
  let progress = 0;

  const iter = {
    next() {
      if (i === ranges.length) {
        return { value: -1, done: true };
      }

      const result = { value: j, done: false };
      j++;
      const r = ranges[i];
      if (j === r.from + r.amount) {
        progress += r.amount;
        console.log(`${((progress / iteratorLength) * 100).toFixed(1)}% tehty`);
        i++;
        j = i < ranges.length ? ranges[i].from : -1;
      }

      return result;
    },
  };

  return iter;
};

const it = makeIterator(seeds_part2);

let answer_part2 = Infinity;
let result = it.next();
while (!result.done) {
  answer_part2 = Math.min(answer_part2, findLocation(result.value));
  result = it.next();
}

console.log("Part 2: " + answer_part2);
