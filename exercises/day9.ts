import fs from "fs";
import _ from "lodash";

const readlines = (path: string) => {
  return fs
    .readFileSync(path, { encoding: "utf8" })
    .split("\n")
    .filter((l) => l.length > 0);
};

const lines = readlines("./inputs/day9.txt");

const input = lines.map((line) => line.split(" ").map((n) => Number(n)));

const handleLineP1 = (line: number[]): number => {
  if (line.every((n) => n === 0)) return 0;

  const zipped = _.zip(line.slice(0, -1), line.slice(1));
  const nextIteration = zipped.map(([a, b]) => b! - a!);

  return handleLineP1(nextIteration) + line[line.length - 1];
};

const answerP1 = input
  .map((line) => handleLineP1(line))
  .reduce((a, b) => a + b);

console.log("Part 1: " + answerP1);

const handleLineP2 = (line: number[]): number => {
  if (line.every((n) => n === 0)) return 0;

  const zipped = _.zip(line.slice(0, -1), line.slice(1));
  const nextIteration = zipped.map(([a, b]) => b! - a!);

  return line[0] - handleLineP2(nextIteration);
};

const answerP2 = input
  .map((line) => handleLineP2(line))
  .reduce((a, b) => a + b);

console.log("Part 2: " + answerP2);
