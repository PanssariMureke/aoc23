import fs from "fs";

const readlines = (path: string) => {
  return fs.readFileSync(path, { encoding: "utf8" }).split("\n");
};

const lines = readlines("../inputs/day2.txt");

const green = /[0-9]+ green/;
const blue = /[0-9]+ blue/;
const red = /[0-9]+ red/;

const games = lines.map((line) => {
  let id = 0;
  try {
    id = Number(line.split(" ")[1].slice(0, -1));
  } catch (e) {
    console.log(line);
  }
  const sets = line.split("; ").map((picked) => ({
    green: Number(picked.match(green)?.[0].split(" ")[0]) || 0,
    blue: Number(picked.match(blue)?.[0].split(" ")[0]) || 0,
    red: Number(picked.match(red)?.[0].split(" ")[0]) || 0,
  }));

  return { id, sets };
});

const filtered = games.filter(
  (game) => !game.sets.some((s) => s.red > 12 || s.green > 13 || s.blue > 14),
);

console.log("Part 1: " + filtered.reduce((a, b) => a + b.id, 0));

const minimums = games.map((game) =>
  game.sets.reduce((a, b) => ({
    red: Math.max(a.red, b.red),
    green: Math.max(a.green, b.green),
    blue: Math.max(a.blue, b.blue),
  })),
);

const powers = minimums.map((game) => game.red * game.green * game.blue);

console.log("Part 2: " + powers.reduce((a, b) => a + b));
