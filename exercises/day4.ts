import fs from "fs";

const readlines = (path: string) => {
  return fs
    .readFileSync(path, { encoding: "utf8" })
    .split("\n")
    .filter((l) => l.length > 0);
};

const lines = readlines("../inputs/day4.txt");

const cards = lines.map((line) => {
  const parts = line.split(" | ");
  const winNumbers = parts[0]
    .split(": ")[1]
    .split(" ")
    .filter((n) => n.length > 0)
    .map((n) => Number(n));
  const numbersYouHave = parts[1]
    .split(" ")
    .filter((n) => n.length > 0)
    .map((n) => Number(n));
  return { winNumbers, numbersYouHave, instances: 1 };
});

const points_part1 = cards.map((card) => {
  const power =
    card.numbersYouHave.filter((n) => card.winNumbers.includes(n)).length - 1;
  return power >= 0 ? Math.pow(2, power) : 0;
});

console.log("Part 1: " + points_part1.reduce((a, b) => a + b));

cards.forEach((card, i) => {
  const hits = card.numbersYouHave.filter((n) =>
    card.winNumbers.includes(n),
  ).length;
  for (let j = i + 1; j <= i + hits; j++) {
    cards[j].instances += card.instances;
  }
});

console.log("Part 2: " + cards.reduce((a, b) => a + b.instances, 0));
