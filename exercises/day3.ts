import fs from "fs";
import _ from "lodash";

type Boundary = {
  min: { x: number; y: number };
  max: { x: number; y: number };
};

type PartNumber = {
  x: number;
  y: number;
  val: number;
};

const readlines = (path: string) => {
  return fs.readFileSync(path, { encoding: "utf8" }).split("\n");
};

const isNumeric = (c: string) => {
  return !isNaN(Number(c));
};

const boundary = (x: number, y: number, len: number): Boundary => {
  return { min: { x: x - 1, y: y - 1 }, max: { x: x + len, y: y + 1 } };
};

const within = (x: number, y: number, bounds: Boundary) => {
  return (
    x >= bounds.min.x &&
    y >= bounds.min.y &&
    x <= bounds.max.x &&
    y <= bounds.max.y
  );
};

const lines = readlines("../inputs/day3.txt");

const grid = lines.map((line) => line.split(""));

const isPartNumber = (x: number, y: number, num: string[]): boolean => {
  const top =
    y > 0
      ? grid[y - 1]
          .slice(
            Math.max(x - 1, 0),
            Math.min(x + num.length + 1, grid[y].length),
          )
          .some((i) => i !== ".")
      : false;
  const bottom =
    y < grid.length - 2
      ? grid[y + 1]
          .slice(
            Math.max(x - 1, 0),
            Math.min(x + num.length + 1, grid[y].length),
          )
          .some((i) => i !== ".")
      : false;

  const left = x > 0 ? grid[y][x - 1] !== "." : false;
  const right =
    x < grid[y].length - num.length ? grid[y][x + num.length] !== "." : false;

  return top || bottom || left || right;
};

const partNums = grid.flatMap((line, y) => {
  const partNumbers: PartNumber[] = [];
  let x = 0;
  while (x < line.length) {
    if (!isNumeric(line[x])) {
      x += 1;
      continue;
    }

    const num = _.takeWhile(line.slice(x), (i) => isNumeric(i));

    if (isPartNumber(x, y, num)) {
      partNumbers.push({ x, y, val: Number(num.join("")) });
    }

    x += num.length;
  }

  return partNumbers;
});

const sum = partNums.reduce((a, b) => a + b.val, 0);

console.log("Part 1: " + sum);

const isGear = (x: number, y: number): [boolean, number] => {
  if (grid[y][x] !== "*") {
    return [false, 0];
  }

  const adjacent: number[] = [];

  partNums.forEach((pn) => {
    const bounds = boundary(pn.x, pn.y, pn.val.toString().length);
    const isIn = within(x, y, bounds);
    if (isIn) {
      adjacent.push(pn.val);
    }
  });

  if (adjacent.length !== 2) {
    return [false, 0];
  }

  return [true, adjacent[0] * adjacent[1]];
};

const gearPowers = lines.flatMap((line, y) => {
  const gears: number[] = [];

  for (let x = 0; x < line.length; x++) {
    const answer = isGear(x, y);
    if (answer[0]) {
      gears.push(answer[1]);
    }
  }

  return gears;
});

console.log("Part 2: " + gearPowers.reduce((a, b) => a + b));
