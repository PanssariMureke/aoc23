import fs from "fs";
import { range } from "lodash";

const readlines = (path: string) => {
  return fs
    .readFileSync(path, { encoding: "utf8" })
    .split("\n")
    .filter((l) => l.length > 0);
};

const isNumeric = (c: string) => c.length > 0 && !isNaN(Number(c));

const lines = readlines("../inputs/day6.txt");

const times = lines[0]
  .split(" ")
  .filter((t) => isNumeric(t))
  .map((t) => Number(t));
const distances = lines[1]
  .split(" ")
  .filter((d) => isNumeric(d))
  .map((d) => Number(d));

const answers = times.map((t, i) => {
  let amountOfLongerDistances = 0;
  range(1, t).forEach((tp) => {
    amountOfLongerDistances += Number((t - tp) * tp > distances[i]);
  });
  return amountOfLongerDistances;
});

console.log("Part 1: " + answers.reduce((a, b) => a * b));

const timeP2 = Number(times.map((t) => t.toFixed()).join(""));
const distanceP2 = Number(distances.map((d) => d.toFixed()).join(""));

let amountOfLongerDistances = 0;
range(1, timeP2).forEach((tp) => {
  amountOfLongerDistances += Number((timeP2 - tp) * tp > distanceP2);
});

console.log("Part 2: " + amountOfLongerDistances);
