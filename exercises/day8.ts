import fs from "fs";

const readlines = (path: string) => {
  return fs
    .readFileSync(path, { encoding: "utf8" })
    .split("\n")
    .filter((l) => l.length > 0);
};

const input = readlines("./inputs/day8.txt");

const instructions = input[0].split("") as Array<"L" | "R">;

const nodes = new Map(
  input.slice(1).map((l) => {
    const matches = l.match(/([A-Z0-9]{3}) = \(([A-Z0-9]{3}), ([A-Z0-9]{3})\)/);
    return [matches![1], { L: matches![2], R: matches![3] }];
  }),
);

// let stepsP1 = 0;
// let i = 0;
// let currentNode = nodes.get("AAA")!;
// while (true) {
//   stepsP1++;
//   const arrivingTo = currentNode[instructions[i]];
//
//   if (arrivingTo === "ZZZ") {
//     break;
//   }
//
//   currentNode = nodes.get(arrivingTo)!;
//   i = (i + 1) % instructions.length;
// }
//
// console.log("Part 1: " + stepsP1);

const solveFor = (
  start: string,
  nodes: Map<string, { L: string; R: string }>,
  instructions: Array<"L" | "R">,
) => {
  let current = start;
  let i = 0;
  let steps = 0;
  while (current[current.length - 1] !== "Z") {
    steps++;
    current = nodes.get(current)![instructions[i]];
    i = (i + 1) % instructions.length;
  }
  return steps;
};

const lcm = (a: number, b: number) => {
  const gcd = (x: number, y: number): number => (!y ? x : gcd(y, x % y));
  return (a * b) / gcd(a, b);
};

let stepsP1 = solveFor("AAA", nodes, instructions);

console.log("Part 1: " + stepsP1);

let stepsP2 = Array.from(nodes.keys())
  .filter((k) => k[k.length - 1] === "A")
  .map((k) => solveFor(k, nodes, instructions))
  .reduce((acc, v) => lcm(acc, v));

console.log("Part 2: " + stepsP2);
